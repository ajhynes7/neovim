return {
	settings = {
		pylsp = {
			plugins = {
				autopep8 = { enabled = false },
				flake8 = { enabled = false },
				jedi_completion = { enabled = false },
				mccabe = { enabled = false },
				pycodestyle = { enabled = false },
				pydocstyle = { enabled = false },
				pyflakes = { enabled = false },
				pylint = { enabled = false },
				rope_autoimport = { enabled = true },
				rope_completion = { enabled = true },
				yapf = { enabled = false },
			},
		},
	},
}
