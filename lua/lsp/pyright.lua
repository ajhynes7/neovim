return {
	settings = {
		python = {
			disableLanguageServices = false,
			disableOrganizeImports = true,
			analysis = {
				autoImportCompletions = true,
				diagnosticMode = "openFilesOnly",
				logLevel = "Error",
				stubPath = vim.fn.stdpath("data") .. "/lazy/python-type-stubs",
				typeCheckingMode = "off",
				useLibraryCodeForTypes = true,
			},
		},
	},
}
