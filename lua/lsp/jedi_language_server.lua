return {
	settings = {
		jedi = {
			enable = true,
			startupMessage = true,
			jediSettings = {
				caseInsensitiveCompletion = true,
				autoImportModules = { "numpy", "pandas" },
			},
			completion = {
				disableSnippets = false,
				resolveEagerly = false,
			},
			diagnostics = {
				enable = true,
			},
			hover = {
				enable = true,
			},
		},
	},
}
