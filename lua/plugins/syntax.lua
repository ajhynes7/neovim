return {
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		config = function()
			require("config.treesitter")
		end,
	},

	"nvim-treesitter/nvim-treesitter",
	"nvim-treesitter/playground",
}
