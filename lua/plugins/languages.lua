return {
	-- TOML
	"cespare/vim-toml",

	-- Python
	"jeetsukumaran/vim-pythonsense",

	-- Julia
	"JuliaEditorSupport/julia-vim",
}
