return {
	{
		"mfussenegger/nvim-lint",
		config = function()
			require("config.nvim-lint")
		end,
	},
	{
		"folke/trouble.nvim",
		-- if some code requires a module from an unloaded plugin, it will be automatically loaded.
		-- So for api plugins like devicons, we can always set lazy=true
		dependencies = { "kyazdani42/nvim-web-devicons", lazy = true },
		config = function()
			require("config.trouble")
		end,
	},
}
