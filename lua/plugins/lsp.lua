return {
	"williamboman/mason.nvim",
	dependencies = {
		"neovim/nvim-lspconfig",
		"folke/neodev.nvim",
	},
	lazy = false,
	config = function()
		require("config.lsp")
	end,
}
