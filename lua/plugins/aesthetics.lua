return {
	{
		"folke/zen-mode.nvim",
		config = function()
			require("zen-mode").setup({})
		end,
	},
	{
		"folke/twilight.nvim",
		config = function()
			require("config.twilight")
		end,
	},
	"norcalli/nvim-colorizer.lua",
	{
		"kyazdani42/nvim-tree.lua",
		dependencies = "kyazdani42/nvim-web-devicons",
		config = function()
			require("config.nvim-tree")
		end,
	},
	{
		"hoob3rt/lualine.nvim",
		config = function()
			require("config.lualine")
		end,
	},
	{
		"akinsho/toggleterm.nvim",
		config = function()
			require("config.toggleterm")
		end,
	},
	{
		"gorbit99/codewindow.nvim",
		config = function()
			require("config.codewindow")
		end,
	},
	{ "shortcuts/no-neck-pain.nvim" },
}
