return {
	{
		"mfussenegger/nvim-dap",
		config = function()
			require("config.dap")
		end,
		dependencies = {
			"mfussenegger/nvim-dap-python",
			"rcarriga/nvim-dap-ui",
		},
	},
}
