return {
	"editorconfig/editorconfig-vim",

	"tpope/vim-surround",
	"tpope/vim-repeat",

	{
		"terrortylor/nvim-comment",
		config = function()
			require("nvim_comment").setup()
		end,
	},

	{
		"kosayoda/nvim-lightbulb",
		dependencies = "antoinemadec/FixCursorHold.nvim",
		config = function()
			require("config.nvim-lightbulb")
		end,
	},

	{ "folke/which-key.nvim", lazy = true },
}
