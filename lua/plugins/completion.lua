return {
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp", -- Source for neovim's builtin LSP client
			"hrsh7th/cmp-buffer", -- Source for buffer words
			"onsails/lspkind.nvim", -- Pictograms for neovim LSP completion items
		},
		lazy = false,
		config = function()
			require("config.nvim-cmp")
		end,
	},
}
