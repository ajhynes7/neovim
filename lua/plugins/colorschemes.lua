return {
	"rebelot/kanagawa.nvim",
	"fenetikm/falcon",
	"savq/melange",
	"EdenEast/nightfox.nvim",
	"navarasu/onedark.nvim",
	"sainnhe/sonokai",
	"Shatur/neovim-ayu",
	{ "catppuccin/nvim", name = "catppuccin" },
}
