require("nvim-treesitter.configs").setup({

	ensure_installed = {
		"bash",
		"dockerfile",
		"graphql",
		"html",
		"javascript",
		"json",
		"julia",
		"lua",
		"python",
		"rust",
		"toml",
		"typescript",
		"yaml",
	},

	highlight = {
		enable = true,
	},

	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = "gnn",
			node_incremental = "grn",
			scope_incremental = "grc",
			node_decremental = "grm",
		},
	},
})
