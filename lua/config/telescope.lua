local telescope = require("telescope")

local plugins = { "fzf", "live_grep_args" }

for _, value in pairs(plugins) do
	telescope.load_extension(value)
end

telescope.setup({
	defaults = {
		vimgrep_arguments = {
			"rg",
			"--color=never",
			"--no-heading",
			"--with-filename",
			"--line-number",
			"--column",
			"--smart-case",
			"--hidden",
			"--glob=!.git/*",
		},
	},
	extensions = {
		live_grep_args = {
			auto_quoting = false,
		},
	},
	pickers = {
		colorscheme = {
			enable_preview = true,
		},
		find_files = {
			find_command = {
				"rg",
				"--files",
				"--hidden",
				"--glob=!.git/*",
			},
		},
	},
})

local wk = require("which-key")
local builtin = require("telescope.builtin")

wk.register({
	name = "Telescope",
	b = {
		function()
			builtin.buffers()
		end,
		"Search open buffers",
	},
	f = {
		function()
			builtin.find_files()
		end,
		"Search all files",
	},
	g = {
		function()
			builtin.git_files()
		end,
		"Search git files",
	},
	s = {
		function()
			builtin.git_status()
		end,
		"Search files in git status",
	},
	c = {
		function()
			builtin.current_buffer_fuzzy_find()
		end,
		"Fuzzy find in current buffer",
	},
	l = {
		function()
			builtin.live_grep()
		end,
		"Live grep",
	},
	la = {
		function()
			require("telescope").extensions.live_grep_args.live_grep_args()
		end,
		"Live grep args",
	},
}, {
	prefix = "<leader>f",
})
