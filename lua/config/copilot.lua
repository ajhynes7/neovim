local wk = require("which-key")
local copilot = require("copilot")
local suggestion = require("copilot.suggestion")

copilot.setup({
	suggestion = { enabled = false, auto_trigger = true },
	panel = { enabled = true },
})

wk.register({
	name = "Copilot",
	a = {
		function()
			suggestion.accept()
		end,
		"Accept all of the suggestion",
	},
	l = {
		function()
			suggestion.accept_line()
		end,
		"Accept line",
	},
	["]"] = {
		function()
			suggestion.next()
		end,
		"Next suggestion",
	},
	["["] = {
		function()
			suggestion.next()
		end,
		"Previous suggestion",
	},
	d = {
		function()
			suggestion.dismiss()
		end,
		"Dismiss suggestion",
	},
}, {
	mode = "i",
	prefix = ",",
})
