local nvim_tree = require("nvim-tree")
local wk = require("which-key")

nvim_tree.setup()

wk.register({
	name = "nvim-tree",
	t = {
		function()
			require("nvim-tree.api").tree.toggle()
		end,
		"Toggle nvim tree",
	},
}, {
	prefix = "<leader>t",
})
