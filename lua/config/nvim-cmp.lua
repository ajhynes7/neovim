local cmp = require("cmp")
local lspkind = require("lspkind")

-- Set completeopt to have a better completion experience
vim.o.completeopt = "menuone,noselect"

cmp.setup({
	mapping = {
		["<Tab>"] = cmp.mapping(cmp.mapping.select_next_item(), { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(cmp.mapping.select_prev_item(), { "i", "s" }),
		["<CR>"] = cmp.mapping.confirm({
			select = true,
			behavior = cmp.ConfirmBehavior.Replace,
		}),
	},
	sources = {
		{ name = "nvim_lsp", group_index = 1 },
		{ name = "buffer", group_index = 2 },
	},
	formatting = {
		format = lspkind.cmp_format({
			mode = "symbol_text",
			menu = {
				nvim_lsp = "[LSP]",
				buffer = "[Buffer]",
			},
		}),
	},
})

-- Use buffer source for `/` and `?`.
cmp.setup.cmdline({ "/", "?" }, {
	mapping = cmp.mapping.preset.cmdline(),
	sources = {
		{ name = "buffer" },
	},
})
