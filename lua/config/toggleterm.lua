local function select_size(term)
	if term.direction == "horizontal" then
		return 20
	elseif term.direction == "vertical" then
		return vim.o.columns * 0.4
	end
end

require("toggleterm").setup({
	shade_terminals = true,
	direction = "horizontal",
	size = select_size,
})
