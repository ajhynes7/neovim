require("conform").setup({
	formatters_by_ft = {
		javascript = { "prettier" },
		json = { "prettier" },
		lua = { "stylua" },
		python = { "ruff_fix", "ruff_format" },
	},
	format_on_save = { lsp_fallback = true },
})
