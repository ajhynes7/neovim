local neotest = require("neotest")

neotest.setup({
	adapters = {
		require("neotest-python")({ runner = "pytest", pytest_discover_instances = true }),
	},
	icons = {
		passed = "✓",
		running = "●",
		failed = "✕",
	},
	discovery = {
		enabled = false,
	},
	quickfix = {
		enabled = false,
	},
	summary = {
		open = "botright vsplit | vertical resize 100",
	},
})

local wk = require("which-key")

wk.register({
	name = "neotest",
	n = {
		function()
			neotest.run.run()
		end,
		"Run nearest test",
	},
	f = {
		function()
			neotest.run.run(vim.fn.expand("%"))
		end,
		"Run all tests in file",
	},
	o = {
		function()
			neotest.output.open()
		end,
		"Open output of a test",
	},
	s = {
		function()
			neotest.summary.open()
		end,
		"Open test summary",
	},
	["]"] = {
		function()
			neotest.jump.next({ status = "failed" })
		end,
		"Jump to next failing test",
	},
	["["] = {
		function()
			neotest.jump.prev({ status = "failed" })
		end,
		"Jump to previous failing test",
	},
}, {
	prefix = "<leader>t",
})
