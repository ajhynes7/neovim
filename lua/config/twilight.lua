require("twilight").setup({
	context = 0,
	treesitter = true,

	-- For treesitter, we always try to expand to the top-most ancestor with these types.
	expand = {
		"class_definition",
		"decorated_definition",
		"function_definition",
	},
})
