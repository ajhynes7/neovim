local gitsigns = require("gitsigns")

gitsigns.setup({})

local wk = require("which-key")

wk.register({
	name = "gitsigns",
	b = {
		function()
			gitsigns.blame_line()
		end,
		"Show blame of line",
	},
	n = {
		function()
			gitsigns.next_hunk()
		end,
		"Jump to next hunk",
	},
	N = {
		function()
			gitsigns.prev_hunk()
		end,
		"Jump to previous hunk",
	},
	p = {
		function()
			gitsigns.preview_hunk()
		end,
		"Preview hunk",
	},
	r = {
		function()
			gitsigns.reset_hunk()
		end,
		"Reset hunk",
	},
}, {
	prefix = "<leader>g",
})
