local wk = require("which-key")

wk.register({
	name = "LazyGit",
	g = {
		":LazyGit<CR>",
		"Open LazyGit",
	},
}, {
	prefix = "<leader>l",
})
