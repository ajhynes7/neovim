require("neodev").setup()
require("mason").setup()

local lsp = require("lsp")
local on_attach = require("lsp.on_attach")
local lspconfig = require("lspconfig")

local server_names = { "pyright" }

for _, server_name in pairs(server_names) do
	local config = lsp[server_name]

	if not config then
		print("No config found for " .. server_name)
	end
	config.on_attach = on_attach

	lspconfig[server_name].setup(config)
end
