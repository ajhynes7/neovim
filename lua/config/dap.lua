local wk = require("which-key")

local dap = require("dap")
local dap_python = require("dap-python")
local dap_ui = require("dapui")

dap_ui.setup({})

dap_python.setup("~/.virtualenvs/debug/bin/python")
dap_python.test_runner = "pytest"

wk.register({
	name = "Debug",
	m = {
		function()
			dap_python.test_method()
		end,
		"Test nearest method",
	},
	u = {
		function()
			dap_ui.toggle()
		end,
		"Toggle UI",
	},
	c = {
		function()
			dap.continue()
		end,
		"Continue",
	},
	b = {
		function()
			dap.toggle_breakpoint()
		end,
		"Toggle Breakpoint",
	},
	q = {
		function()
			dap.close()
		end,
		"Close Debugger",
	},
}, {
	prefix = "<leader>d",
})

wk.register({
	name = "Step",
	o = {
		function()
			dap.step_over()
		end,
		"Step Over",
	},
	i = {
		function()
			dap.step_into()
		end,
		"Step Into",
	},
	u = {
		function()
			dap.step_out()
		end,
		"Step Out",
	},
}, {
	prefix = "<leader>ds",
})

wk.register({
	name = "REPL",
	c = {
		function()
			vim.bo.modifiable = true
			vim.api.nvim_buf_set_lines(0, 0, -1, true, {})
		end,
		"Clear",
	},
}, {
	prefix = "<leader>dr",
})
