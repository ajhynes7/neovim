local api = vim.api -- Neovim API
local cmd = vim.cmd -- Execute vim commands
local opt = vim.opt -- Helper for vim configurations
local g = vim.g

local opts = { noremap = true, silent = true }
api.nvim_set_keymap("n", "<esc>", ":noh<CR>", opts)

-- Escape terminal mode
api.nvim_set_keymap("t", "<Esc>", [[<C-\><C-n>]], opts)

opt.relativenumber = true
opt.termguicolors = true

cmd("set number")
cmd("set makeprg=mypy\\ .")

g.mapleader = " "

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins")

-- Colorscheme
g.sonokai_style = "andromeda"
g.sonokai_enable_italic = 1
g.sonokai_disable_italic_comment = 1

require("ayu").setup({
	mirage = true,
})

cmd("colorscheme ayu")

require("nvim-web-devicons").get_icons()
