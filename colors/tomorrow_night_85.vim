" Maintainer: Andrew Hynes <andrewjhynes@gmail.com>

set background=dark
hi clear
if exists('syntax_on')
  syntax reset
endif
let g:colors_name='tomorrow_night_85'

hi Normal guifg=#cccccc ctermfg=252 guibg=#2d2d2d ctermbg=236 gui=NONE cterm=NONE
hi ColorColumn guifg=NONE ctermfg=NONE guibg=#515151 ctermbg=239 gui=NONE cterm=NONE
hi Conceal guifg=#b4b7b4 ctermfg=249 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CursorColumn guifg=NONE ctermfg=NONE guibg=#515151 ctermbg=239 gui=NONE cterm=NONE
hi CursorLine guifg=NONE ctermfg=NONE guibg=#515151 ctermbg=239 gui=NONE cterm=NONE
hi CursorLineNr guifg=#ffffff ctermfg=231 guibg=NONE ctermbg=NONE gui=bold cterm=bold
hi DiffAdd guifg=#393939 ctermfg=237 guibg=#99cc99 ctermbg=114 gui=NONE cterm=NONE
hi DiffChange guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=underline cterm=underline
hi DiffDelete guifg=#393939 ctermfg=237 guibg=#f2777a ctermbg=210 gui=NONE cterm=NONE
hi DiffText guifg=#393939 ctermfg=237 guibg=#ffcc66 ctermbg=221 gui=NONE cterm=NONE
hi Directory guifg=#6699cc ctermfg=68 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi EndOfBuffer guifg=#393939 ctermfg=237 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi ErrorMsg guifg=#f2777a ctermfg=210 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi FoldColumn guifg=#515151 ctermfg=239 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Folded guifg=#b4b7b4 ctermfg=249 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi IncSearch guifg=#ffcc66 ctermfg=221 guibg=#b4b7b4 ctermbg=249 gui=NONE cterm=NONE
hi LineNr guifg=#cccccc ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi MatchParen guifg=#6699cc ctermfg=68 guibg=NONE ctermbg=NONE gui=underline cterm=underline
hi NonText guifg=#b4b7b4 ctermfg=249 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Pmenu guifg=#ffffff ctermfg=231 guibg=#999999 ctermbg=247 gui=NONE cterm=NONE
hi PmenuSbar guifg=NONE ctermfg=NONE guibg=#999999 ctermbg=247 gui=NONE cterm=NONE
hi PmenuSel guifg=#393939 ctermfg=237 guibg=#6699cc ctermbg=68 gui=NONE cterm=NONE
hi PmenuThumb guifg=NONE ctermfg=NONE guibg=#ffffff ctermbg=231 gui=NONE cterm=NONE
hi Question guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi QuickFixLine guifg=#393939 ctermfg=237 guibg=#ffcc66 ctermbg=221 gui=NONE cterm=NONE
hi Search guifg=#393939 ctermfg=237 guibg=#ffcc66 ctermbg=221 gui=NONE cterm=NONE
hi SignColumn guifg=NONE ctermfg=NONE guibg=#2d2d2d ctermbg=236 gui=NONE cterm=NONE
hi SpecialKey guifg=#999999 ctermfg=247 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellBad guifg=#f2777a ctermfg=210 guibg=NONE ctermbg=NONE gui=underline cterm=underline
hi SpellCap guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellLocal guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellRare guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StatusLine guifg=#ffffff ctermfg=231 guibg=#515151 ctermbg=239 gui=NONE cterm=NONE
hi StatusLineNC guifg=#b4b7b4 ctermfg=249 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StatusLineTerm guifg=#ffffff ctermfg=231 guibg=#515151 ctermbg=239 gui=NONE cterm=NONE
hi StatusLineTermNC guifg=#515151 ctermfg=239 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLine guifg=#b4b7b4 ctermfg=249 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TabLineFill guifg=NONE ctermfg=NONE guibg=#2d2d2d ctermbg=236 gui=NONE cterm=NONE
hi TabLineSel guifg=#ffffff ctermfg=231 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Terminal guifg=#ffffff ctermfg=231 guibg=#393939 ctermbg=237 gui=NONE cterm=NONE
hi VertSplit guifg=#f2777a ctermfg=210 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Visual guifg=NONE ctermfg=NONE guibg=#999999 ctermbg=247 gui=NONE cterm=NONE
hi VisualNOS guifg=#999999 ctermfg=247 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi WarningMsg guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi WildMenu guifg=#393939 ctermfg=237 guibg=#6699cc ctermbg=68 gui=NONE cterm=NONE
hi TSError guifg=#f2777a ctermfg=210 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSPunctDelimiter guifg=#cccccc ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSPunctBracket guifg=#cccccc ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSPunctSpecial guifg=#cccccc ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSComment guifg=#999999 ctermfg=247 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSConstant guifg=#f2777a ctermfg=210 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSBoolean guifg=#66cccc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSCharacter guifg=#99cc99 ctermfg=114 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSConstBuiltin guifg=#f99157 ctermfg=209 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSFloat guifg=#6699cc ctermfg=68 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSNumber guifg=#66cccc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSString guifg=#99cc99 ctermfg=114 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSStringRegex guifg=#99cc99 ctermfg=114 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSFuncBuiltin guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSFuncMacro guifg=#6699cc ctermfg=68 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSFunction guifg=#6699cc ctermfg=68 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSMethod guifg=#6699cc ctermfg=68 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSParameter guifg=#cccccc ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSField guifg=#f2777a ctermfg=210 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSConditional guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSException guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSInclude guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSKeyword guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSKeywordFunction guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSKeywordOperator guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSOperator guifg=#cccccc ctermfg=252 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSRepeat guifg=#cc99cc ctermfg=176 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSType guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi TSTypeBuiltin guifg=#ffcc66 ctermfg=221 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
